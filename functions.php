<?php 
    include ('database.php');
    $post = $_POST;
    $functions = new functions; 

    if (isset($post['method']) && method_exists($functions, $post['method']) ) {
        call_user_func(array($functions, $post['method']), $post);
        die();
    }

    if (isset($_GET['method']) && method_exists($functions, $_GET['method']) ) {
        call_user_func(array($functions, $_GET['method']), $_GET);
        die();
    }
    

    class functions {
        function __construct() 
        {
            session_start();
            $this->con = mysqli_connect("localhost","root","","delpilar");
        }

        function login($post) 
        { 
            if (!empty($post['username']) && !empty($post['password'])) {
                $result = mysqli_query($this->con,"select * from tbl_users where username = '{$post['username']}' and password = '{$post['password']}'");

                if (mysqli_num_rows($result) == 1) {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $_SESSION['user_id'] = $row['id'];
                    $_SESSION['first_name'] = $row['first_name'];
                    $_SESSION['last_name'] = $row['last_name'];
                    $_SESSION['user_type'] = $row['usertype'];
                    header('Location: home.php');
                } else {
                    header('Location: login.php?error=2');
                }
            } else {
                header('Location: login.php?error=1');
            }
        }

        function register($post)
        {
            if (empty($post['username']) || empty($post['password']) || empty($post['first_name']) || empty($post['last_name']) || empty($post['dob']) || empty($post['gender']) || empty($post['address'])) {
                header('Location: register.php?error=1');
            }
            
            $existing_username = mysqli_query($this->con,"select * from tbl_users where username = '{$post['username']}'");
            
            if (mysqli_num_rows($existing_username) > 0) {
                header('Location: register.php?error=2');
            }

            $data = array(
                'username'   => $post['username'],
                'password'   => $post['password'],
                'first_name' => $post['first_name'],
                'last_name'  => $post['last_name'],
                'address'    => $post['address'],
                'gender'     => $post['gender'],
                'dob'        => $post['dob'],
                'usertype'   => 2
            );
            if ( insert('tbl_users', $data) ) {
                header('Location: register.php?success=1');
            }
        }

        function requestClearance($post) {
            if (isset($post['purpose']) && !empty($post['purpose'])) {
                $data = array(
                    'requested_by'      => $_SESSION['user_id'],
                    'purpose'           => $post['purpose'],
                    'date_requested'    => date('Y-m-d')
                );

                if ( insert('tbl_clearance', $data) ) {
                    header('Location: requestclearance.php?success=1');
                }
            } else {
                header('Location: requestclearance.php?error=1');
            }
        }

        function addBlotter($post) {
            if ( empty($post['first_name']) || empty($post['last_name']) || empty($post['first_name']) || empty($post['reason']) ) {
                header('Location: addblotter.php?error=1');
            } else {
                $data = array(
                    'first_name'    => $post['first_name'],
                    'last_name'     => $post['last_name'],
                    'reason'        => $post['reason'],
                    'reported_by'   => $_SESSION['user_id'],
                    'date_reported' => date('Y-m-d')
                );

                if ( insert('tbl_blotter', $data) ) {
                    header('Location: addblotter.php?success=1');
                }
            }
        }

        function process_clearance($post){
            if (update('tbl_clearance',array('is_processed' => 1), 'id = '.$post['id'])){
                header('Location: clearance.php');
            }
        }

        function process_blotter($post){
            if (update('tbl_blotter',array('is_processed' => 1), 'id = '.$post['id'])){
                header('Location: blotter.php');
            }
        }

        function delete_clearance($post){
            if (delete('tbl_clearance', 'id = '.$post['id'])){
                header('Location: clearance.php');
            }
        }

        function delete_blotter($post){
            if (delete('tbl_blotter', 'id = '.$post['id'])){
                header('Location: blotter.php');
            }
        }

        function addEvent($post) {
            if (empty($post['title']) || empty($post['description']) || empty($post['date'])) {
                header('Location: addevents.php?error=1');
            } else {
                $data = array(
                    'name'        => $post['title'],
                    'description' => $post['description'],
                    'date'        => $post['date']
                );

                if ( insert('tbl_events', $data) ) {
                    header('Location: addevents.php?success=1');
                }
            }
        }

        function delete_event($post) {
            if (delete('tbl_events', 'id = '.$post['id'])){
                header('Location: events.php');
            }
        }
    }

    function _a($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }

    function insert($table, $data) {
        $functions = new functions; 
        $fields = array();
        $values = array();
        
        foreach ($data as $field => $value) {
            $fields[] = $field;
            $values[] = '"'.$value.'"';
        }
        $sql = 'INSERT INTO '.$table.' ('.implode(", ",$fields).') VALUES ('.implode(",",$values).')';
        
        if ($functions->con->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . $functions->con->error;
            die();
        }
    }

    function update ($table, $data, $where) {
        $functions = new functions; 
        $fields = array();
        $values = array();
        
        $sql = 'UPDATE '.$table;

        $set = array();
        foreach ($data as $field => $value) {
            $set[] = $field.' =  "'.$value.'"';
        }

        $sql .= ' SET '.implode(", ",$set);
        
        $sql .= ' WHERE '.$where;

        // die($sql);
        
        if ($functions->con->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . $functions->con->error;
            die();
        }
    }

    function delete($table, $where) {
        $functions = new functions; 
        $sql = 'Delete from '.$table;
        $sql .= ' WHERE '.$where;

        if ($functions->con->query($sql) === TRUE) {
            return true;
        } else {
            echo "Error: " . $sql . "<br>" . $functions->con->error;
            die();
        }
    }
?>