<?php include('template/header.php'); ?>
<?php include('session_checker.php'); ?>

<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <?php 
            $error_message = '';
            if ( isset($_GET['error']) ) {
                switch ($_GET['error']){
                    case 1: 
                        $error_message = 'PURPOSE is required';
                        break;
                }
            }

            if (!empty($error_message)) {
                echo '<div class="alert alert-danger">
                    <strong>ERROR!</strong> '.$error_message.'
                </div>';
            }

            if ( isset($_GET['success']) ) {
                echo '<div class="alert alert-success">
                    <strong>SUCCESS!</strong>Clearance Requested
                </div>';
            }
        ?>
        <div class="row">
            <div class="col-sm-6">
                <form method="post" action="functions.php">
                    <input type="hidden" name="method" value="requestClearance">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Reason</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="purpose"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="float:right">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<?php include('template/footer.php'); ?>