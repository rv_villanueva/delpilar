<?php include('template/header.php'); ?>
<?php include('database.php'); ?>

<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <div class="row pull-right">
            <?php 
                if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1){
                    echo '<a href="addevents.php"><button type="submit" class="btn btn-primary" style="float:right">Add Event</button></a>';
                }
            ?>
        </div>
        <div class="row" style="margin-top:20px">
            <div class="col-sm-12">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date</th>
                        <?php 
                        if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1){
                        echo '<th>Action</th>';
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $query = "select * from tbl_events";
                        
                        $results = mysqli_query($conn, $query);
                        while($result = mysqli_fetch_assoc($results)) {
                            echo '<tr>
                                    <td>'.$result['name'].'</td>
                                    <td>'.$result['description'].'</td>
                                    <td>'.$result['date'].'</td>';
                            if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1){
                                echo '<td>';
                                echo '<a href="functions.php?method=delete_event&id='.$result['id'].'"><button class="btn btn-primary">Delete</button></a>';
                                echo '</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php include('template/footer.php'); ?>