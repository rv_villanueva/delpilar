<?php include('template/header.php'); ?>
<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
<?php include('session_checker.php'); ?>

<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <?php 
            $error_message = '';
            if ( isset($_GET['error']) ) {
                switch ($_GET['error']){
                    case 1: 
                        $error_message = 'All fields are required';
                        break;
                }
            }

            if (!empty($error_message)) {
                echo '<div class="alert alert-danger">
                    <strong>ERROR!</strong> '.$error_message.'
                </div>';
            }

            if ( isset($_GET['success']) ) {
                echo '<div class="alert alert-success">
                    <strong>SUCCESS!</strong>Event Added
                </div>';
            }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="functions.php">
                    <input type="hidden" name="method" value="addEvent">
                    <div class="form-group ">
                        <label class="col-sm-12 col-form-label">Title</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-form-label">Description</label>
                        <div class="col-sm-12">
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-form-label">Date</label>
                        <div class="col-sm-12">
                            <input name="date" id="date" type="text" class="form-control" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<?php include('template/footer.php'); ?>
<script src="jquery-ui/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    } );
</script>