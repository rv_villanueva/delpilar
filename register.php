<?php include('template/header.php'); ?>
<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <?php 
            $error_message = '';
            if ( isset($_GET['error']) ) {
                switch ($_GET['error']){
                    case 1: 
                        $error_message = 'All fields are required';
                        break;
                    case 2: 
                        $error_message = 'Username already exists';
                        break;
                }
            }

            if (!empty($error_message)) {
                echo '<div class="alert alert-danger">
                    <strong>ERROR!</strong> '.$error_message.'
                </div>';
            }

            if ( isset($_GET['success']) ) {
                echo '<div class="alert alert-success">
                    <strong>SUCCESS!</strong> Successfully Registered
                </div>';
            }
        ?>
        <div class="row">
            <div class="col-sm-8">
                <form method="post" action="functions.php">
                    <input type="hidden" name="method" value="register">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">UserName</label>
                        <div class="col-sm-10">
                            <input name="username" type="text" class="form-control" placeholder="UserName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">First Name</label>
                        <div class="col-sm-10">
                            <input name="first_name" type="text" class="form-control" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Last Name</label>
                        <div class="col-sm-10">
                            <input name="last_name" type="text" class="form-control" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Date of Birth</label>
                        <div class="col-sm-10">
                            <input name="dob" id="dob" type="text" class="form-control" placeholder="Date of Birth">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Gender</label>
                        <div class="col-sm-10">
                            <select name="gender" class="form-control">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="float:right">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php include('template/footer.php'); ?>
<script src="jquery-ui/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#dob" ).datepicker({ dateFormat: 'yy-mm-dd' });
    } );
</script>