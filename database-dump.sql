/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.1.37 : Database - delpilar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`delpilar` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `delpilar`;

/*Table structure for table `tbl_blotter` */

DROP TABLE IF EXISTS `tbl_blotter`;

CREATE TABLE `tbl_blotter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `reason` varchar(1000) DEFAULT NULL,
  `reported_by` int(11) DEFAULT NULL,
  `date_reported` varchar(100) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_blotter` */

insert  into `tbl_blotter`(`id`,`first_name`,`last_name`,`reason`,`reported_by`,`date_reported`,`is_processed`) values (2,'test','asdfafsdf','test',1,'2017-08-21',0);

/*Table structure for table `tbl_clearance` */

DROP TABLE IF EXISTS `tbl_clearance`;

CREATE TABLE `tbl_clearance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requested_by` int(11) DEFAULT NULL,
  `purpose` varchar(1000) DEFAULT NULL,
  `date_requested` varchar(100) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_clearance` */

insert  into `tbl_clearance`(`id`,`requested_by`,`purpose`,`date_requested`,`is_processed`) values (5,1,'asdf123','2017-08-20',1);

/*Table structure for table `tbl_events` */

DROP TABLE IF EXISTS `tbl_events`;

CREATE TABLE `tbl_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_events` */

insert  into `tbl_events`(`id`,`name`,`description`,`date`) values (2,'test','test','2017-08-31');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `usertype` enum('1','2') DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`id`,`username`,`password`,`first_name`,`last_name`,`address`,`gender`,`dob`,`usertype`) values (1,'arvee','arvee','raphael','villanueva','jaen',NULL,NULL,'2'),(2,'username','password','firstname','lastname','address na mahaba','male','2017-08-31','2'),(4,'asdf','asdf','asdf','asdf','asdf','male','2017-08-31','2'),(5,'admin','admin','admin','admin','admin','male',NULL,'1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
