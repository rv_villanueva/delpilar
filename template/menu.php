<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/delpilar">Sample Website Name</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <a class="nav-link" href="events.php">Events</a>
            <?php
                $url = explode('/',$_SERVER["PHP_SELF"]);
                if($url[2] != 'login.php') {
                    if (!isset($_SESSION["user_id"]) || empty($_SESSION["user_id"])) {
                        echo '<li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>';
                    } else {
                        if (isset($_SESSION["user_type"]) || $_SESSION['user_type'] == 2) {
                            echo '<a class="nav-link" href="clearance.php">Clearance</a>';
                            echo '<a class="nav-link" href="blotter.php">Blotter</a>';
                        }
                        echo '<a class="nav-link" href="logout.php">Logout</a>';
                    }
                }
            ?>
        </ul>
    </div>
</nav>