<?php include('database.php'); ?>
<?php include('template/header.php'); ?>

<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <?php 
            $error_message = '';
            if ( isset($_GET['error']) ) {
                switch ($_GET['error']){
                    case 1: 
                        $error_message = 'USERNAME and PASSWORD are required';
                        break;
                    case 2: 
                        $error_message = 'Incorect Username or Password';
                        break;
                }
            }

            if (!empty($error_message)) {
                echo '<div class="alert alert-danger">
                    <strong>ERROR!</strong> '.$error_message.'
                </div>';
            }
        ?>
        <div class="row">
            <div class="col-sm-6">
                <form method="post" action="functions.php">
                    <input type="hidden" name="method" value="login">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">UserName</label>
                        <div class="col-sm-10">
                            <input name="username" type="text" class="form-control" placeholder="UserName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <span>Doesn't have an account? Click <a href="register.php">here</a> to register.</span>
                        <button type="submit" class="btn btn-primary" style="float:right">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<?php include('template/footer.php'); ?>