<?php include('template/header.php'); ?>
<?php include('session_checker.php'); ?>
<?php include('database.php'); ?>

<body>
    <?php include('template/menu.php'); ?>
    <div class="container" style="margin-top:20px">
        <div class="row pull-right">
            <?php 
                if ($_SESSION['user_type'] != 1){
                    echo '<a href="addblotter.php"><button type="submit" class="btn btn-primary" style="float:right">Report Blotter</button></a>';
                }
            ?>
        </div>
        <div class="row" style="margin-top:20px">
            <div class="col-sm-12">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Reason</th>
                        <th>Date Reported</th>
                        <th>Status</th>
                        <?php 
                        if ($_SESSION['user_type'] == 1){
                            echo '<th>Reported By</th>';
                            echo '<th>Action</th>';
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $query = "select * from tbl_blotter where reported_by = '{$_SESSION['user_id']}'";
                        if ($_SESSION['user_type'] == 1){
                            $query = "SELECT tbl_blotter.*,CONCAT(tbl_users.`first_name`,' ',tbl_users.`last_name`) AS `reported_by` FROM tbl_blotter INNER JOIN tbl_users ON tbl_blotter.`reported_by` = tbl_users.`id`";
                        }
                        $results = mysqli_query($conn, $query);
                        while($result = mysqli_fetch_assoc($results)) {
                            echo '<tr>
                                    <td>'.$result['first_name'].' '.$result['last_name'].'</td>
                                    <td>'.$result['reason'].'</td>
                                    <td>'.$result['date_reported'].'</td>
                                    <td>'.($result['is_processed'] == 1 ? 'Processed' : 'Pending').'</td>';
                            if ($_SESSION['user_type'] == 1){
                                echo '<td>'.$result['reported_by'].'</td>';
                                echo '<td>';
                                if ($result['is_processed'] != 1) {
                                    echo '<a href="functions.php?method=process_blotter&id='.$result['id'].'"><button type="submit" class="btn btn-primary">Process</button></a>';
                                }
                                echo '<a style="margin-left:10px" href="functions.php?method=delete_blotter&id='.$result['id'].'"><button type="submit" class="btn btn-primary">Delete</button></a>
                                    </td>';
                            }
                            echo  '</tr>';
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php include('template/footer.php'); ?>